#!/bin/bash
#SBATCH --mem=32G
#SBATCH -J MNase-peakcall
#SBATCH -o MNase-peakcall-%J

# This script is adapted bash script of commands called by the ENCODE pipeline

export LC_COLLATE=C

# Inputs:

INFILE=$1
OUTROOT=${INFILE%.*}

# MACS2 peak calling

 macs2 callpeak \
			-t $INFILE -f AUTO -n $OUTROOT -g "1371719383" -p 0.01 \
			--nomodel --extsize 150 -B --SPMR --keep-dup all \
			--call-summits

# sorting

 sort -k 8gr,8gr ${OUTROOT}_peaks.narrowPeak |\
  awk 'BEGIN{OFS="\t"}{$4="Peak_"NR ; print $0}' |\
   gzip -nc > ${OUTROOT}.narrowPeak.gz

# removing non-sorted and redundant files

 rm -f ${OUTROOT}_peaks.narrowPeak

 rm -f ${OUTROOT}_peaks.xls

 rm -f ${OUTROOT}_summits.bed

# calculating the library size factor

 sval=$(wc -l <(zcat -f "$INFILE") | awk '{printf "%f", $1/1000000}')

# generating signal over background signal

 macs2 bdgcmp \
			-t ${OUTROOT}_treat_pileup.bdg -c ${OUTROOT}_control_lambda.bdg \
			--o-prefix $OUTROOT -m ppois -S "${sval}"

# cleaning up

 slopBed -i ${OUTROOT}_ppois.bdg -g "danRer10.chrom.sizes" -b 0 |\
  bedClip stdin "danRer10.chrom.sizes" ${OUTROOT}.pval.signal.bedgraph

# more cleaning up

 rm -f ${OUTROOT}_ppois.bdg

# sorting bedGraph

 sort -k1,1 -k2,2n ${OUTROOT}.pval.signal.bedgraph > ${OUTROOT}.pval.signal.srt.bedgraph

# creating bigWig from bedGraph

 bedGraphToBigWig ${OUTROOT}.pval.signal.srt.bedgraph "danRer10.chrom.sizes" ${OUTROOT}.pval.signal.bigwig

# optional cleaning up, I disabled it not to delete the intermediate files if something goes wrong

# rm -f ${OUTROOT}.pval.signal.bedgraph ${OUTROOT}.pval.signal.srt.bedgraph

# rm -f ${OUTROOT}_treat_pileup.bdg ${OUTROOT}_control_lambda.bdg

