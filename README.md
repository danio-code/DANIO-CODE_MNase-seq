# DANIO-CODE MNase-seq pipeline v1.0

MNase-seq is a sequencing technique used in molecular biology, in which DNA is the input molecule derived from a micrococcal nuclease digestion followed by high throughput sequencing, A method that distinguishes nucleosome positioning based on the ability of nucleosomes to protect associated DNA from digestion by micrococcal nuclease. Sequenced fragments reveal nucleosome location information in chromatin regions of the input DNA. (Source: EBI)

## Description of the pipeline

The pipeline is an adapted version of the ATAC-Seq / DNase-Seq Pipeline developed at Kundaje lab (https://github.com/kundajelab/atac_dnase_pipelines).
MNase-Seq data processing methods are very similar to ChIP-seq, ATAC-seq or DNA-seq, with the difference that fragments length are extended to 146 bp, in order to match the fragment's real size.
The pipeline consists of the following steps:

* Quality filtering and trimming of raw reads (Trim-galore)
* Mapping reads to the reference genome (Bowtie2)
* Signal generation (MACS2)

Details of the pipeline, scripts, and references are accessible on GitLab: https://gitlab.com/danio-code/DANIO-CODE_MNase-seq

## Outputs:
The output of the pipeline is a *bigWig* file of signals across the genome. The genome is generated as the negative logarithm of p-value calculated as the probability of observed signal over expected signal drawn from the Poisson distribution. 


